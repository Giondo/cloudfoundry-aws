module "cloudformation_stack" {
  source = "git::https://github.com/cloudposse/terraform-aws-cloudformation-stack.git?ref=master"

  enabled            = true
  namespace          = "eg"
  stage              = "prod"
  name               = "Pivotal-Cloud-Foundry"
  template_url       = "https://aws-quickstart.s3.amazonaws.com/quickstart-pivotal-cloudfoundry/templates/pivotal-cloudfoundry.template"

  parameters         = {
    PCFKeyPair = aws_key_pair.pcfkey.key_name
    SSLCertificateARN = aws_acm_certificate.maincert.arn
    # ElbPrefix = "CloudFoundry"
    OpsManagerIngress = "0.0.0.0/0"
    HostedZoneId = aws_route53_zone.primary.zone_id
    Domain = "${var.SUBDOMAIN}.${var.DOMAIN}"
    DeploymentSize = "SmallFootPrint"
    PivnetToken = var.PIVNETTOKEN
    AdminEmail = var.CLOUDFLARE_EMAIL
    OpsManagerAdminPassword = "asdjhask1223123asdSDDA"
    QSS3KeyPrefix = "quickstart-pivotal-cloudfoundry"
    QSS3BucketName = "aws-quickstart"
    AcceptEULA = "Yes"
  }

  capabilities = ["CAPABILITY_IAM"]

  depends_on = [aws_acm_certificate_validation.cert]
}
