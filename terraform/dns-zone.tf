# Create managed zone in aws using Route53 Service
resource "aws_route53_zone" "primary" {
  name = "${var.SUBDOMAIN}.${var.DOMAIN}"
  force_destroy = true
}
