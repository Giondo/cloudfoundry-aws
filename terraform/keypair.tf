resource "aws_key_pair" "pcfkey" {
   public_key = file("~/.ssh/id_rsa.pub")
   key_name = "pcfkey"
 }
