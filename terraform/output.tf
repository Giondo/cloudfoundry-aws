output "Zone-NS" {
	value = aws_route53_zone.primary.name_servers
}
output "certarn" {
	value = aws_acm_certificate.maincert.arn
}
