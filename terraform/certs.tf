# This creates an SSL certificate
resource "aws_acm_certificate" "maincert" {
  domain_name       = "*.${var.SUBDOMAIN}.${var.DOMAIN}"
  subject_alternative_names = tolist(null_resource.certaltnames.*.triggers.subject_alternative_names)
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

resource "null_resource" "certaltnames" {
  count = length(var.PASSDOMAINS)
  triggers = {
    subject_alternative_names = "*.${tostring(element(var.PASSDOMAINS, count.index))}.${var.SUBDOMAIN}.${var.DOMAIN}"
  }
}


# This is a DNS record for the ACM certificate validation to prove we own the domain
#
# This example, we make an assumption that the certificate is for a single domain name so can just use the first value of the
# domain_validation_options.  It allows the terraform to apply without having to be targeted.
# This is somewhat less complex than the example at https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate_validation
# - that above example, won't apply without targeting

resource "aws_route53_record" "cert_validation" {
  count = length(var.PASSDOMAINS) + 1
  allow_overwrite = true
  name            = tolist(aws_acm_certificate.maincert.domain_validation_options)[count.index].resource_record_name
  records         = [ tolist(aws_acm_certificate.maincert.domain_validation_options)[count.index].resource_record_value ]
  type            = tolist(aws_acm_certificate.maincert.domain_validation_options)[count.index].resource_record_type
  zone_id  = aws_route53_zone.primary.id
  ttl      = 60
  depends_on = [aws_acm_certificate.maincert]
}


# This tells terraform to cause the route53 validation to happen
resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.maincert.arn
  validation_record_fqdns = [for record in aws_route53_record.cert_validation : record.fqdn]
  depends_on = [aws_route53_record.cert_validation]
}
