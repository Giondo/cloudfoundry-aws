provider "aws" {
  region     = "us-west-2"
  access_key = var.ACCESS_KEY
  secret_key = var.SECRET_KEY
}

provider "cloudflare" {
  version = "~> 2.0"
  email = var.CLOUDFLARE_EMAIL
  api_key = var.CLOUDFLARE_TOKEN
}
