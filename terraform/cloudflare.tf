data "cloudflare_zones" "domain" {
  filter {
    name = var.DOMAIN
  }
  # depends_on = [aws_route53_zone.primary]
}
# Add a record to the domain
resource "cloudflare_record" "nssubdomain" {
  # count = length(aws_route53_zone.primary.name_servers)
  # Cannot use count and depend_on issue https://github.com/hashicorp/terraform/issues/26078
  # We asume that there always will be 4 name_servers
  count = 4
  zone_id = data.cloudflare_zones.domain.zones[0].id
  name    = var.SUBDOMAIN
  type    = "NS"
  value = element(aws_route53_zone.primary.name_servers, count.index)

  depends_on = [aws_route53_zone.primary]
}
