# Deploy a Pivotal Cloud Foundry on AWS (for testing purposes) aka Small Footprint PAS
## the automated way

## How to use it


### CloudFlare API

You will need to get the Global api_key for CloudFlare
(it seems there is an issue reading the zones with the personal keys)

### Fill out all vars

Check and fill out all variables on vars.tf

## Requirements
* terraform >= 0.13
* cloudflare plugin ~> 2.0

## References

* https://aws.amazon.com/quickstart/architecture/pivotal-cloud-foundry/
